# Cucumber-starter

## introduction

An empty intellij IDEA project configured for entering Cucumber tests 
using Java 17 and Gradle.

Fork or remove the .git map to start your own git project.

## adding feature files

Put your xxx.feature files under src\test\resources\features

## adding test code

Put your Cucumber testcode under src\test\java in package be.kdg.agenda
( If your step definitions (tests) are under another package, specify 
it in the glue attribute of be.kdg.agenda.RunCucumberTest)

## Running tests

Once your features and tests have been written you can run your tests
by right clicking your feature file and run it

## cucmber and spring
You can find the setup for running cucumber with spring in the spring branch