package be.kdg.agenda;

import static org.junit.jupiter.api.Assertions.assertEquals;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.*;
import java.util.HashMap;
import java.util.Map;

public class CheckWeekendSteps {
 private WeekDay weekDay;
 private boolean isWeekend;
 private Map<String,Boolean> dateOracle ;
  private String dayString;





// this code is run before every test
  @Given("days")
  public void days(DataTable dataTable) {
    // Write code here that turns the phrase above into concrete actions
    // For automatic transformation, change DataTable to one of
    // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
    // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
    // Double, Byte, Short, Long, BigInteger or BigDecimal.
    //
    // For other transformations you can register a DataTableType.
    dateOracle = new HashMap<>();
    for (Map<String, String> line : dataTable.asMaps()) {
      dateOracle.put(line.get("day"),line.get("answer").equals("yes"));
    }
  }

  @Given("today is {int}-{int}-{int}")
  public void todayIs(int year, int month, int day) {
    weekDay = new WeekDay(year, month, day);
  }

  //Act
  @When("I check if it is weekend")
  public void iCheckIfItIsWeekend() {
    isWeekend = weekDay.isWeekend();
  }

  //Assert
  @Then("the answer should be {string}")
  public void theAnswerShouldBe(String answer) {
    assertEquals(isWeekend, answer.equals("YES"));
  }


  @Given("the day of today is {string}")
  public void todayIs(String day) {
    dayString=day;
    weekDay = new WeekDay(day);
  }

  @Then("the answer should be correct")
  public void theAnswerShouldBeCorrect() {
    assertEquals(dateOracle.get(dayString),isWeekend);
  }

}
