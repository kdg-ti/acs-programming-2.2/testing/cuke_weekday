package be.kdg.agenda;


import org.junit.platform.suite.api.*;

import static io.cucumber.core.options.Constants.PLUGIN_PROPERTY_NAME;


/**
 * @author Jan de Rijke.
 */
@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("features")
@ConfigurationParameter(key = PLUGIN_PROPERTY_NAME, value = "pretty")

// use this if your cucumber test classes are not in the same package as the CucumberRunner class
// supports a comma separated list of values
// can also be specified in junit-platform.properties
// @ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "be.kdg.pro4.agenda")
public class RunCucumberTest {
}


