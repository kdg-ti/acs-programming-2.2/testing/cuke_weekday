# write your gherkin acceptance criteria in a xxx.feature file
  Feature: As a user I want to be able to see what day of the week it is

    # note the date 2022-12-32 will cause an error message "invalid date", but that's on purpose
    Background:
      Given days
       | day        | answer |
       | 2022-12-20 | no     |
       | 2022-12-25 | yes    |
       | 2022-12-32 | no     |

      # check based on <day> from background table and verified against Example answers
    Scenario Outline: Check that today is not part of the weekend
      Given today is <day>
      When I check if it is weekend
      Then the answer should be "<answer>"
      Examples:
        | day        | answer |
        | 2022-12-20 | NO     |
        | 2022-12-25 | YES    |
        | 2022-12-32 | NO     |



      # check based on literal date and match with answer from background
    Scenario: Check that today is part of the weekend
      Given the day of today is "2022-12-25"
      When I check if it is weekend
      Then the answer should be correct
