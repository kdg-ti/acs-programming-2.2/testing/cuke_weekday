package be.kdg.agenda;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class WeekDay {
  LocalDate date;
  public WeekDay(String date){
    try {
      this.date = LocalDate.parse(date);
    } catch (Exception e) {
      System.err.println("Invalid date format. Date will be null");
    }
  }

  public WeekDay(int year, int month, int day) {
    try {
      this.date = LocalDate.of(year,month,day);
    } catch (Exception e) {
      System.err.println("Invalid date format. Date will be null");
    }
  }

  public boolean isWeekend(){
    if (date == null) return false;

    DayOfWeek dayOfWeek = date.getDayOfWeek();
    return dayOfWeek.equals(DayOfWeek.SUNDAY)||dayOfWeek.equals(DayOfWeek.SATURDAY);
  }

}
